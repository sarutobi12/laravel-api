<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserssesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userss', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('username')->nullable();
            $table->string('alias')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('fullname')->nullable();
            $table->boolean('state')->nullable();
            $table->integer('levelID');
            $table->integer('role');
            $table->boolean('isActive');
            $table->integer('permission');
            $table->binary('passwordHash')->nullable();
            $table->binary('passwordSalt')->nullable();
            $table->string('createdBy')->nullable();
            $table->integer('userID');
            $table->string('modifyBy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersses');
    }
}
