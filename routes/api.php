<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use phpDocumentor\Reflection\Types\Resource_;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// =======================================================
Route::post('/register', 'Api\AuthController@register');
Route::post('/login', 'Api\AuthController@login');

// Route::get('/getAllCeo', 'Api\CEOController@index')->middleware('auth:api');middleware de xac thuc controller
Route::group([
    'prefix' => 'Ceo'
    // 'as' => 'api.',
    // 'middleware' => ['auth:api']
], function () {
    //lists all Ceo
    Route::get('/getAllCeo', 'Api\CEOController@index');

    //add new Ceo
    Route::post('/AddCeo', 'Api\CEOController@store');

    //get Ceo by ID
    Route::get('/getbyID/{id}', 'Api\CEOController@show');

    //Update Ceo
    Route::put('/update', 'Api\CEOController@update');

    //delete Ceo
    Route::delete('/delete/{id}', 'Api\CEOController@destroy');
});

Route::group([
    'prefix' => 'AdminLevel'
    // 'as' => 'api.'
    // 'middleware' => ['auth:api']
], function () {
    //lists all Ceo

    Route::get('/getListTree', 'Api\AdminLevelController@GetListTree');

    Route::post('/addOrUpdate', 'Api\AdminLevelController@addOrUpdate');
});
