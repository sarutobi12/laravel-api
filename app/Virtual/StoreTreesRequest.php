<?php

/**
 * @OA\Schema(
 *      title="Store Tree request",
 *      description="Store  Tree request body data",
 *      type="object",
 *      required={"name"}
 * )
 */

class StoreTreesRequest
{
    /**
     * @OA\Property(
     *      title="ID",
     *      description="ID of the Tree",
     *      example=0
     * )
     *
     * @var integer
     */
    public $ID;


    /**
     * @OA\Property(
     *      title="name",
     *      description="Name of the Tree",
     * )
     *
     * @var string
     */
    public $Name;


    /**
     * @OA\Property(
     *      title="Code",
     *      description="Code of the Tree",
     * )
     *
     * @var string
     */
    public $Code;



    /**
     * @OA\Property(
     *      title="ParentID",
     *      description="ParentID of the new Tree",
     *      example=0
     * )
     *
     * @var integer
     */
    public $ParentID;

    /**
     * @OA\Property(
     *      title="ParentCode",
     *      description="ParentCode id of the Tree",
     * )
     *
     * @var string
     */
    public $ParentCode;

    /**
     * @OA\Property(
     *      title="LevelNumber",
     *      description="LevelNumber  of the tree",
     *      example=1
     * )
     *
     * @var integer
     */
    public $LevelNumber;

    /**
     * @OA\Property(
     *      title="State",
     *      description="State  of the tree",
     *      example=false
     * )
     *
     * @var boolean
     */
    public $State;

    /**
     * @OA\Property(
     *     title="CreateTime",
     *     description="CreateTime",
     *     example="2020-01-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    public $CreateTime;
}
