<?php

/**
 * @OA\Schema(
 *      title="Store Ceo Update request",
 *      description="Store Project request body data",
 *      type="object",
 *      required={"name"}
 * )
 */

class StoreCeosUpdateRequest
{
    /**
     * @OA\Property(
     *      title="id",
     *      description="id of the CEO",
     *      example="1"
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *      title="name",
     *      description="Name of the CEO",
     *      example="ABCXYZ"
     * )
     *
     * @var string
     */
    public $name;


    /**
     * @OA\Property(
     *      title="company_name",
     *      description="company_name of the CEO",
     *      example="Shyang Hung Cheng"
     * )
     *
     * @var string
     */
    public $company_name;



    /**
     * @OA\Property(
     *      title="year",
     *      description="year of the new CEO",
     *      example="2020"
     * )
     *
     * @var integer
     */
    public $year;

    /**
     * @OA\Property(
     *      title="company_headquarters",
     *      description="company_headquarters id of the CEO",
     *      example="Shyang Hung Cheng"
     * )
     *
     * @var string
     */
    public $company_headquarters;

    /**
     * @OA\Property(
     *      title="what_company_does",
     *      description="what_company_does id of the CEO",
     *      example="Beacause...."
     * )
     *
     * @var string
     */
    public $what_company_does;
}
