<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Level",
 *     description="Admin model Level",
 *     @OA\Xml(
 *         name="Level"
 *     )
 * )
 */
class TreeView
{

    /**
     * @OA\Property(
     *     title="key",
     *     description="key",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $key;

    /**
     * @OA\Property(
     *      title="title",
     *      description="title",
     *      example="title",
     * )
     *
     * @var string
     */
    public $title;

    /**
     * @OA\Property(
     *      title="code",
     *      description="code",
     *      example="code"
     * )
     *
     * @var string
     */
    public $code;

    /**
     * @OA\Property(
     *     title="levelnumber",
     *     description="levelnumber",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $levelnumber;


    /**
     * @OA\Property(
     *     title="parentid",
     *     description="parentid",
     *     format="int64",
     *     example="2020-01-27 17:50:45",
     * )
     *
     * @var integer
     */
    public $parentid;

    /**
     * @OA\Property(
     *     title="state",
     *     description="state",
     *     format="boolean",
     *     example=true,
     * )
     *
     * @var boolean
     */
    public $state;

    /**
     * @OA\Property(
     *     title="HasChildren",
     *     description="HasChildren",
     *     format="boolean",
     *     example=true,
     * )
     *
     * @var boolean
     */
    public $HasChildren;

    /**
     * @OA\Property(
     *     title="children",
     *     description="children",
     * )
     *
     * @var \App\Virtual\Models\TreeView[]
     */
    public $children;


}
