<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="KPILevel",
 *     description="Admin model Level",
 *     @OA\Xml(
 *         name="Level"
 *     )
 * )
 */
class KPILevel
{


    public  $ID ;
    public  $KPILevelCode ;
    public  $UserCheck ;
    public  $KPIID ;
    public  $LevelID ;
    public  $TeamID ;
    public  $Period ;
    public  $Weekly ;




}
