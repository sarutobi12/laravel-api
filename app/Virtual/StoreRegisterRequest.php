<?php

/**
 * @OA\Schema(
 *      title="Store Register request",
 *      description="Store Project request body data",
 *      type="object",
 *      required={"name"}
 * )
 */

class StoreRegisterRequest
{
    /**
     * @OA\Property(
     *      title="name",
     *      description="Name of the new project",
     *      example="leo"
     * )
     *
     * @var string
     */
    public $name;



    /**
     * @OA\Property(
     *      title="email",
     *      description="email of the new project",
     *      example="leo.doan@shc.ssbshoes.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="password",
     *      description="password's id of the new project",
     *      example="shc@1234"
     * )
     *
     * @var string
     */
    public $password;
}
