<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KPI extends Model
{
    //
    protected $table ='KPIs';
    public $timestamps = false;

}
