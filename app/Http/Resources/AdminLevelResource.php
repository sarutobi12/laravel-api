<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminLevelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
        // return [
        //     'key' => $request->ID,
        //     'title' => $request->title,
        //     'code' => $request->code,
        //     'state' => $request->state,
        //     'levelnumber' => $request->levelnumber,
        //     'parentid' => $request->parentid,
        // ];
    }
}
