<?php

namespace App\Http\Controllers\API;

use App\CEO;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\CEOResource;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class CEOController extends Controller
{


    /**
     * @OA\Get(
     * path="/api/Ceo/getAllCeo",
     * operationId="getCeoList",
     * tags={"Ceos"},
     * security={
     *  {"passport": {}}},
     * summary="Get list of CEO",
     * description="Returns list of Ceos",
     * @OA\Response(response=200, description="Successful operation", @OA\MediaType( mediaType="application/json")),
     * @OA\Response(response=401, description="Unauthenticated"),
     * @OA\Response(response=403, description="Forbidden"),
     * @OA\Response(response=400, description="Bad Request"),
     * @OA\Response(response=404, description="not found"),
     *  )
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ceos = CEO::all();
        return response(
        [
            'status' => 'true',
            'status_code' => Response::HTTP_OK,
            'data' => CEOResource::collection($ceos),
            'message' => 'Retrieved successfully'
        ], 200);
    }



    /**
     * @OA\Post(
     ** path="/api/Ceo/AddCeo",
     *   tags={"Ceos"},
     *   summary="AddCeo",
     *   operationId="AddCeo",
     *   @OA\RequestBody(required=true, @OA\JsonContent(ref="#/components/schemas/StoreCeosRequest")),
     *   @OA\Response(response=201, description="Success", @OA\JsonContent(ref="#/components/schemas/StoreCeosRequest")),
     *   @OA\Response(response=401, description="Unauthenticated"),
     *   @OA\Response(response=400, description="Bad Request"),
     *   @OA\Response(response=404, description="not found"),
     *   @OA\Response(response=403, description="Forbidden"))
     */

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'year' => 'required|max:255',
            'company_headquarters' => 'required|max:255',
            'what_company_does' => 'required'
        ]);

        if($validator->fails()){
            return response(['error' => $validator->errors(), 'Validation Error']);
        }

        $ceo = CEO::create($data);

        return response(
        [
            'ceo' => new CEOResource($ceo),
            'status' => 'true',
            'status_code' => Response::HTTP_OK,
            'message' => 'Created successfully'

        ], 200);
    }


    /**
     * @OA\Get(
     *   path="/api/Ceo/getbyID/{id}",
     *   operationId="GetCeoByID",
     *   tags={"Ceos"},
     *   summary="Get CEO by ID",
     *   description="Returns CEO by ID",
     *
     * @OA\Parameter(
     *    name="id",
     *    in="path",
     *    required=true,
     *    @OA\Schema(type="integer")),
     * @OA\Response(response=200, description="Success", @OA\MediaType(mediaType="application/json",)),
     * @OA\Response(response=401, description="Unauthenticated",),
     * @OA\Response(response=403, description="Forbidden"),
     * @OA\Response(response=400, description="Bad Request"),
     * @OA\Response(response=404, description="not found"),)
     */

    /**
     * Display the specified resource.
     *
     * @param  \App\CEO  $cEO
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // if (CEO::where('id', $id)->exists()) {
        //     $ceos = CEO::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
        //     return response($ceos, 200);
        // } else {
        //     return response()->json([
        //         "message" => "Ceo not found"
        //     ], 404);
        // }
        //
        $ceos = CEO::find($id);
        return response(
        [
            'data' => new CEOResource($ceos),
            'status' => 'true',
            'message' => 'Retrieved successfully'
        ], 200);
    }



    /**
     * @OA\Put(
     ** path="/api/Ceo/update",
     *   tags={"Ceos"},
     *   summary="Update Ceo",
     *   operationId="Update Ceo",
     * @OA\RequestBody(
     *   required=true,
     *   @OA\JsonContent(ref="#/components/schemas/StoreCeosUpdateRequest")
     * ),
     * @OA\Response(
     *   response=200,
     *   description="Success",
     *   @OA\MediaType(mediaType="application/json")
     * ),
     * @OA\Response(response=401, description="Unauthenticated"),
     * @OA\Response(response=400, description="Bad Request"),
     * @OA\Response(response=404, description="not found"),
     * @OA\Response(response=403, description="Forbidden"))
     *
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CEO  $cEO
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $editCeo= CEO::find($request->id);
        $editCeo->update($request->all());

        return response(
        [
            'ceo' => new CEOResource($editCeo),
            'status' => 'success',
            'status_code' => Response::HTTP_OK,
            'message' => 'Retrieved successfully'
        ], 200);
    }


    /**
     * @OA\Delete(
     ** path="/api/Ceo/delete/{id}",
     *  operationId="Delete Ceo",
     *  tags={"Ceos"},
     *  summary="Delete Ceo",
     *  description="Delete Ceo",
     *
     * @OA\Parameter(
     *    name="id",
     *    in="path",
     *    required=true,
     *    @OA\Schema(type="integer")
     * ),
     * @OA\Response(
     *  response=200,
     *  description="Success",
     *  @OA\MediaType(mediaType="application/json")
     * ),
     * @OA\Response(response=401, description="Unauthenticated"),
     * @OA\Response(response=403, description="Forbidden"),
     * @OA\Response(response=400, description="Bad Request"),
     * @OA\Response(response=404, description="not found"))
     *
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CEO  $cEO
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $ceo = CEO::find($id);
        $ceo->delete();

        return response(
            [
                'status' => 'success',
                'status_code' => Response::HTTP_OK,
                'message' => 'Deleted'
            ]);
    }
}
