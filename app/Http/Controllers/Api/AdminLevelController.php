<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdminLevelResource;
use App\Models\KPI;
use App\Models\KPILevel as ModelsKPILevel;
use App\Models\Levels;
use App\Virtual\Models\KPILevel;
use App\Virtual\Models\TreeView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

use function Psy\debug;

class AdminLevelController extends Controller
{
    /**
     * @OA\Get(
     * path="/api/AdminLevel/getListTree",
     * operationId="getListTree",
     * tags={"AdminLevel"},
     * summary="Get list Tree",
     * description="Returns list Tree",
     * @OA\Response(response=200, description="Successful operation", @OA\MediaType(mediaType="application/json",)),
     * @OA\Response(response=401, description="Unauthenticated"),
     * @OA\Response(response=403, description="Forbidden"),
     * @OA\Response(response=400, description="Bad Request"),
     * @OA\Response(response=404, description="not found"),
     *  )
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function GetListTree()
    {
        $listlevel = Levels::all();
        $levels = array();
        foreach ( $listlevel as $listLevels)
        {
            $levelItem = new TreeView();
            $levelItem->key = $listLevels->ID;
            $levelItem->title = $listLevels->Name;
            $levelItem->code = $listLevels->Code;
            $levelItem->state = $listLevels->State;
            $levelItem->levelnumber = $listLevels->LevelNumber;
            $levelItem->parentid = $listLevels->ParentID;
            array_push($levels,$levelItem);
        }
        $data = $levels;
        $query = collect($data)->where('parentid',0);
        $maps = $query->map(function ($items, $key) use ($data){
            $result =  new TreeView();
            $result->key = $items->key;
            $result->title = $items->title;
            $result->code = $items->code;
            $result->levelnumber = $items->levelnumber;
            $result->parentid = $items->parentid;
            $result->state = $items->state;
            $result->HasChildren = $items->HasChildren;
            $result->children = $this->GetChildren($data,$items->key);
            return $result;
        });
        return $maps->values()->toArray();
        // return response() -> json($data);
        return response()->json([
            'data' => $maps
        ]);
        // return new ProjectResource($array);
        // return response(
        //     [
        //         'status' => 'true',
        //         'status_code' => Response::HTTP_OK,
        //         'data' => AdminLevelResource::collection($maps),
        //         'message' => 'Retrieved successfully'
        //     ], 200);
    }

    public function GetChildren($data,$parentid)
    {
       $query = collect($data)->where('parentid',$parentid);
       $map = $query->map(function ($items, $key) use ($data)
        {
            $result =  new TreeView();
            $result->key = $items->key;
            $result->title = $items->title;
            $result->code = $items->code;
            $result->levelnumber = $items->levelnumber;
            $result->parentid = $items->parentid;
            $result->children = $this->GetChildren($data,$items->key);
            return $result;
        });
        return $map->values()->toArray();
    }



     /**
         * @OA\Post(
         ** path="/api/AdminLevel/addOrUpdate",
        *   tags={"AdminLevel"},
        *   summary="Add Or Update Tree",
        *   operationId="Add Or Update Tree",
        *   @OA\RequestBody(required=true, @OA\JsonContent(ref="#/components/schemas/StoreTreesRequest")),
        *   @OA\Response(response=201, description="Success", @OA\JsonContent(ref="#/components/schemas/StoreTreesRequest")),
        *   @OA\Response(response=401, description="Unauthenticated"),
        *   @OA\Response(response=400, description="Bad Request"),
        *   @OA\Response(response=404, description="not found"),
        *   @OA\Response(response=403, description="Forbidden"))
        */

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \App\Models\Levels  $lEvels
         * @return \Illuminate\Http\Response
     */

    public function addOrUpdate(Request $request)
    {
        // $request->Code = strtoupper($request->Code);
        if ($request->ID == 0) {
            // if (DB::table('Levels')->where('Code','=',strtoupper($request->Code)) !='') {
            //     return response()->json([
            //         'message' => 'Failse'
            //     ]);
            // } else {

            // }
            $level = new Levels();
            $level->Name = $request->Name;
            $level->Code = $request->Code;
            $level->LevelNumber = $request->LevelNumber;
            $level->ParentCode = $request->ParentCode;
            $level->State = $request->State;
            $level->ParentID = $request->ParentID;
            $level->CreateTime = $request->CreateTime;
            $level->save();
            $kpiLevelList = new ModelsKPILevel();
            $tamp = array();
            $kpiVM = KPI::all();
            foreach ( $kpiVM as $kpiVMs)
            {
                $kpilevel = new KPILevel();
                $kpilevel->LevelID = $level->id;
                $kpilevel->KPIID = $kpiVMs->ID;
                array_push($tamp,$kpilevel);

                // $kpiLevelList->push($kpilevel);
            }
            // var_dump($kpiLevelList);
            // $collection = collect($tamp);
            // $kpiLevelList = $collection;
            $kpiLevelList->save();


            return response()->json([
                'message' => 'true'
            ],200);

        } else {
            # code...
        }

    }

}
